package es.upm.dit.apsv.cris.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.upm.dit.apsv.cris.dao.PublicationDAO;
import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.dataset.CSV2DB;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;


class TestPublicationDAO {

	private Publication p;
	private PublicationDAO pdao;
	private Researcher r;
	
	@BeforeAll
	static void dbSetUp() throws Exception {
		// Setup initial database data
		CSV2DB.loadPublicationsFromCSV("publications.csv");
		CSV2DB.loadResearchersFromCSV("researchers.csv");
	}
	
	@BeforeEach
		// Executed before each test
	void setUp() throws Exception {
		pdao = PublicationDAOImplementation.getInstance();
		p = new Publication();
		p.setId("85019352728");
		p.setTitle("Structural composites for multifunctional applications: Current challenges and future trends");
		p.setPublicationName("Progress in Materials Science");
		p.setPublicationDate("2017-08-01");
		p.setAuthors("7403372571;23101342100;6602977547;55500167800;55708653400");
		p.setCiteCount(0);
		
        r = new Researcher();
        r.setEmail("7403372571");
        r.setId("7403372571");
        r.setLastname("Gonzalez");
        r.setName("Carlos D.");
        r.setPassword("1234");
        r.setScopusURL("https://www.scopus.com/authid/detail.uri?authorId=7403372571");
	}
	
	@Test
	void testCreate() {
        pdao.delete(p);
        pdao.create(p);
        assertEquals(p, pdao.read(p.getId()));
	}
	
	@Test
	void testRead() {
	        assertEquals(p, pdao.read(p.getId()));
	}
	
	@Test
	void testUpdate() {
	        String oldPubName = p.getPublicationName();
	        p.setPublicationName("IEEE");
	        pdao.update(p);
	        assertEquals(p, pdao.read(p.getId()));
	        p.setPublicationName(oldPubName);
	        pdao.update(p);
	}
	
	@Test
	void testDelete() {
	        pdao.delete(p);
	        assertNull(pdao.read(p.getId()));
	        pdao.create(p);
	}
	
	@Test
	// Check the publications (specifically publications id) of the researcher with id=7403372571
	void testReadAll() {
			// Publications id of Researcher id=7403372571 
			String []ids = {"85019352728","34548108157","33847038743","34250694266","33747329411","22944442210",
		              "9944264957","4544284195","3042749646","2442480662","0037903396","0037454499",
		              "0035834204","0034346731","0343517490","0030194266","85034957208"};
			List<String> lpi = Arrays.asList(ids);
			
			List<String> l = new ArrayList<String>();
			for (Publication i : pdao.readAllPublications(r.getId()))
				l.add(i.getId());
			
			assertEquals(lpi, l);
	}

}
